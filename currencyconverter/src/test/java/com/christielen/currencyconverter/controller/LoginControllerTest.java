package com.christielen.currencyconverter.controller;

import com.christielen.currencyconverter.entity.User;
import com.christielen.currencyconverter.repository.UserRepository;
import com.christielen.currencyconverter.rest.LoginController;
import com.christielen.currencyconverter.security.jwt.AccountCredentials;
import com.christielen.currencyconverter.security.jwt.JwtTokenProvider;
import com.christielen.currencyconverter.security.jwt.Kong;
import com.google.gson.Gson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest(LoginController.class)
public class LoginControllerTest {

    @MockBean
    JwtTokenProvider jwtTokenProvider;

    @MockBean
    UserRepository userRepository;

    @MockBean
    private Kong kong;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void given_credential_return_token() throws Exception {

        AccountCredentials accountCredentials = new AccountCredentials();
        accountCredentials.setUsername("christielen");
        accountCredentials.setPassword("chris123");

        User user = new User();
        user.setUsername("christielen");
        user.setId(new ObjectId("62142146ed8f72171d07ae2e"));

        Map<String,String> hashMap =  new HashMap<>();

        String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjaHJpc3RpZWxlbiIsImlzcyI6Im1UdWFmZ3h6TW9WblN4Q0JGOWxlSkswVmJSQ0NpNDE5IiwiaWF0IjoxNjQ1NDc0MTU2LCJleHAiOjE2NDU1NjA1NTZ9.GZg8hY5eKM1GwS7clfYeipB-_0AofHYDfzCoT6qDTwc";

        when(userRepository.findByUsernameAndPassword(accountCredentials.getUsername(), accountCredentials.getPassword())).thenReturn(user);
        when(jwtTokenProvider.createToken(accountCredentials.getUsername())).thenReturn(token);
        when(kong.criarConsumerJWT(accountCredentials.getUsername())).thenReturn(hashMap);

        Gson gson = new Gson();
        String requestJson = gson.toJson(accountCredentials);

        mockMvc.perform(
                post("/login")
                        .content(requestJson)
                        .contentType("application/json")
        ).andExpect(status().isOk())
         .andExpect(content().string(token));
    }
}
