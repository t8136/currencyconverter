package com.christielen.currencyconverter.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.christielen.currencyconverter.rest.CurrencyConverterController;
import com.christielen.currencyconverter.rest.LoginController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ControllersTest {

    @Autowired
    private CurrencyConverterController currencyConverterController;

    @Autowired
    private LoginController loginController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(loginController).isNotNull();
        assertThat(currencyConverterController).isNotNull();
    }
}
