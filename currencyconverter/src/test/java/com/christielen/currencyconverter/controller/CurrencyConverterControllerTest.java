package com.christielen.currencyconverter.controller;

import com.christielen.currencyconverter.entity.User;
import com.christielen.currencyconverter.json.CurrencyConverted;
import com.christielen.currencyconverter.json.CurrencyToConvert;
import com.christielen.currencyconverter.repository.TransactionRepository;
import com.christielen.currencyconverter.repository.UserRepository;
import com.christielen.currencyconverter.rest.CurrencyConverterController;
import com.christielen.currencyconverter.security.jwt.JwtTokenProvider;
import com.christielen.currencyconverter.service.CurrencyConverterService;
import com.google.gson.Gson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(CurrencyConverterController.class)
public class CurrencyConverterControllerTest {

    @MockBean
    private CurrencyConverterService currencyConverterService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private JwtTokenProvider jwtTokenProvider;

    @MockBean
    private TransactionRepository transactionRepository;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void given_expectValues_addTransaction_returnJson() throws Exception {
        User user = new User();
        user.setUsername("christielen");
        user.setId(new ObjectId("62142146ed8f72171d07ae2e"));

        CurrencyToConvert currencyToConvert = new CurrencyToConvert();
        currencyToConvert.setFrom("EUR");
        currencyToConvert.setTo("BRL");
        currencyToConvert.setAmount(new BigDecimal(5));

        CurrencyConverted currencyConverted =
                new CurrencyConverted.CurrencyConvertedBuilder("62142146ed8f72171d07ae2e", "62142201b33bc93bc9135f12")
                .fromCurrency("EUR")
                .fromAmount(new BigDecimal(5))
                .toCurrency("BRL")
                .toAmount(new BigDecimal(25))
                .rate(new BigDecimal(5)).build();

        String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjaHJpc3RpZWxlbiIsImlzcyI6Im1UdWFmZ3h6TW9WblN4Q0JGOWxlSkswVmJSQ0NpNDE5IiwiaWF0IjoxNjQ1NDc0MTU2LCJleHAiOjE2NDU1NjA1NTZ9.GZg8hY5eKM1GwS7clfYeipB-_0AofHYDfzCoT6qDTwc";

        when(jwtTokenProvider.buscarLogin(token)).thenReturn("christielen");
        when(userRepository.findByUsername("christielen")).thenReturn(user);

        when(currencyConverterService.process(currencyToConvert, user)).thenReturn(currencyConverted);

        Gson gson = new Gson();
        String requestJson = gson.toJson(currencyToConvert);

        mockMvc.perform(
                post("/currencyConverter/process")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                .content(requestJson)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionId").value("62142146ed8f72171d07ae2e"))
                .andExpect(jsonPath("$.userId").value("62142201b33bc93bc9135f12"))
                .andExpect(jsonPath("$.fromCurrency").value("EUR"))
                .andExpect(jsonPath("$.fromAmount").value(new BigDecimal(5)))
                .andExpect(jsonPath("$.toCurrency").value("BRL"))
                .andExpect(jsonPath("$.toAmount").value(new BigDecimal(25)))
                .andExpect(jsonPath("$.rate").value(new BigDecimal(5)));
    }

    @Test
    public void given_expectValues_then_returnTransactions() throws Exception {
        User user = new User();
        user.setUsername("christielen");
        user.setId(new ObjectId("62142146ed8f72171d07ae2e"));

        List<CurrencyConverted> list = new ArrayList<>();
        CurrencyConverted currencyConverted =
                new CurrencyConverted.CurrencyConvertedBuilder("62142146ed8f72171d07ae2e", "62142201b33bc93bc9135f12")
                        .fromCurrency("EUR")
                        .fromAmount(new BigDecimal(5))
                        .toCurrency("BRL")
                        .toAmount(new BigDecimal(25))
                        .rate(new BigDecimal(5)).build();
        list.add(currencyConverted);

        String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjaHJpc3RpZWxlbiIsImlzcyI6Im1UdWFmZ3h6TW9WblN4Q0JGOWxlSkswVmJSQ0NpNDE5IiwiaWF0IjoxNjQ1NDc0MTU2LCJleHAiOjE2NDU1NjA1NTZ9.GZg8hY5eKM1GwS7clfYeipB-_0AofHYDfzCoT6qDTwc";

        when(jwtTokenProvider.buscarLogin(token)).thenReturn("christielen");
        when(userRepository.findByUsername("christielen")).thenReturn(user);

        when(currencyConverterService.findByUser(user)).thenReturn(list);

        this.mockMvc.perform(
                get("/currencyConverter")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                .contentType("application/json")
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$[0].transactionId").value("62142146ed8f72171d07ae2e"))
         .andExpect(jsonPath("$[0].userId").value("62142201b33bc93bc9135f12"))
         .andExpect(jsonPath("$[0].fromCurrency").value("EUR"))
         .andExpect(jsonPath("$[0].fromAmount").value(new BigDecimal(5)))
         .andExpect(jsonPath("$[0].toCurrency").value("BRL"))
         .andExpect(jsonPath("$[0].toAmount").value(new BigDecimal(25)))
         .andExpect(jsonPath("$[0].rate").value(new BigDecimal(5)));
    }
}
