package com.christielen.currencyconverter.service;

import com.christielen.currencyconverter.entity.User;
import com.christielen.currencyconverter.json.CurrencyConverted;
import com.christielen.currencyconverter.json.CurrencyToConvert;
import com.christielen.currencyconverter.json.ExchangeRateApi;
import com.christielen.currencyconverter.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CurrencyConverterServiceTest {

    @Autowired
    private CurrencyConverterService currencyConverterService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void given_expectCountryByContract_then_success() throws IOException, InterruptedException {
        ExchangeRateApi exchangeRateApi = currencyConverterService.getRates("EUR");
        assertThat(exchangeRateApi).isNotNull();
        assertThat(exchangeRateApi.getSuccess()).isEqualTo(true);
    }

    @Test
    public void given_unexpectCountryByContract_then_failed() throws IOException, InterruptedException {
        ExchangeRateApi exchangeRateApi = currencyConverterService.getRates("BRL");
        assertThat(exchangeRateApi).isNotNull();
        assertThat(exchangeRateApi.getSuccess()).isEqualTo(false);
    }

    @Test
    public void given_expectValues_then_success() throws Exception {
        User user = userRepository.findByUsername("christielen");
        CurrencyToConvert currencyToConvert = new CurrencyToConvert();
        currencyToConvert.setAmount(new BigDecimal(5));
        currencyToConvert.setFrom("EUR");
        currencyToConvert.setTo("BRL");
        CurrencyConverted currencyConverted = currencyConverterService.process(currencyToConvert, user);
        assertThat(currencyConverted).isNotNull();
        assertThat(currencyConverted.getToAmount()).isNotNull();
    }

    @Test
    public void given_username_then_return_notnulllist() {
        List<CurrencyConverted> list = currencyConverterService.findByUser(userRepository.findByUsername("christielen"));
        assertThat(list).isNotNull();
    }
}
