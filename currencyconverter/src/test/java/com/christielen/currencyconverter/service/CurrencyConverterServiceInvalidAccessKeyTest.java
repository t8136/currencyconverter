package com.christielen.currencyconverter.service;

import com.christielen.currencyconverter.json.ExchangeRateApi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@TestPropertySource( properties =
        { "exchageRateApi.accessKey=cr5435234545" }
)
@SpringBootTest
public class CurrencyConverterServiceInvalidAccessKeyTest {

    @Autowired
    private CurrencyConverterService currencyConverterService;

    @Test
    public void given_invalidAccessKey_then_failed() throws IOException, InterruptedException {
        ExchangeRateApi exchangeRateApi = currencyConverterService.getRates("EUR");
        assertThat(exchangeRateApi).isNotNull();
        assertThat(exchangeRateApi.getSuccess()).isEqualTo(false);
        assertThat(exchangeRateApi.getError().get("code")).isEqualTo("101");
        assertThat(exchangeRateApi.getError().get("type")).isEqualTo("invalid_access_key");
    }
}
