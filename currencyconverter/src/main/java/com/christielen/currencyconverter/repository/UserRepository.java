package com.christielen.currencyconverter.repository;

import com.christielen.currencyconverter.entity.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, ObjectId> {

    User findByUsernameAndPassword(String username, String password);

    User findByUsername(String username);
}
