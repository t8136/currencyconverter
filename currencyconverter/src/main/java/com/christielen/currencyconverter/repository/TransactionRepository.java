package com.christielen.currencyconverter.repository;

import com.christielen.currencyconverter.entity.Transaction;
import com.christielen.currencyconverter.entity.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TransactionRepository extends MongoRepository<Transaction, ObjectId> {

    @Query(value="{ 'user.$id' : ?0 }")
    public List<Transaction> findByUser(ObjectId id);
}
