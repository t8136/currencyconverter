package com.christielen.currencyconverter.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class CurrencyConverted implements Serializable {

	@JsonProperty("transactionId")
	private String transactionId;

	@JsonProperty("userId")
	private String userId;

	@JsonProperty("fromCurrency")
	private String fromCurrency;

	@JsonProperty("fromAmount")
	private BigDecimal fromAmount;

	@JsonProperty("toCurrency")
	private String toCurrency;

	@JsonProperty("toAmount")
	private BigDecimal toAmount;

	@JsonProperty("rate")
	private BigDecimal rate;

	@JsonProperty("date")
	private LocalDateTime date;

	private CurrencyConverted(CurrencyConvertedBuilder builder) {
		this.transactionId = builder.transactionId;
		this.userId = builder.userId;
		this.fromAmount = builder.fromAmount;
		this.fromCurrency = builder.fromCurrency;
		this.toCurrency = builder.toCurrency;
		this.toAmount = builder.toAmount;
		this.rate = builder.rate;
		this.date = LocalDateTime.now();
	}

	public static class CurrencyConvertedBuilder	{
		private final String transactionId;
		private final String userId;
		private String fromCurrency;
		private BigDecimal fromAmount;
		private String toCurrency;
		private BigDecimal toAmount;
		private BigDecimal rate;

		public CurrencyConvertedBuilder(String transactionId, String userId) {
			this.transactionId = transactionId;
			this.userId = userId;
		}

		public CurrencyConvertedBuilder fromCurrency(String fromCurrency) {
			this.fromCurrency = fromCurrency;
			return this;
		}

		public CurrencyConvertedBuilder fromAmount(BigDecimal fromAmount) {
			this.fromAmount = fromAmount;
			return this;
		}

		public CurrencyConvertedBuilder toCurrency(String toCurrency) {
			this.toCurrency = toCurrency;
			return this;
		}

		public CurrencyConvertedBuilder toAmount(BigDecimal toAmount) {
			this.toAmount = toAmount;
			return this;
		}

		public CurrencyConvertedBuilder rate(BigDecimal rate) {
			this.rate = rate;
			return this;
		}

		public CurrencyConverted build() {
			CurrencyConverted currencyConverted =  new CurrencyConverted(this);
			return currencyConverted;
		}
	}
}