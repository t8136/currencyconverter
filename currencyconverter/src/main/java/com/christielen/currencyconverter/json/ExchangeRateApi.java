package com.christielen.currencyconverter.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
public class ExchangeRateApi implements Serializable {

	@JsonProperty("success")
	private Boolean success;

	@JsonProperty("timestamp")
	private String timestamp;

	@JsonProperty("base")
	private String base;

	@JsonProperty("timestamp")
	private String date;

	@JsonProperty("rates")
	private HashMap<String, BigDecimal> rates;

	@JsonProperty("error")
	private HashMap<String, String> error;
}