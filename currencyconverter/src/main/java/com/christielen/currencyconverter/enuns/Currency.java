package com.christielen.currencyconverter.enuns;

import lombok.Getter;

@Getter
public enum Currency {

    BRL("BRL"),
    USD("USD"),
    EUR("EUR"),
    JPY("JPY");

    public String code;

    Currency(String code){
        this.code = code;
    }
}
