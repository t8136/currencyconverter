package com.christielen.currencyconverter.entity;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Document(collection = "transaction")
@Getter
@Setter
public class Transaction {

	@Id
	private ObjectId id;

	@DBRef
	private User user;

	private String fromCurrency;
	private BigDecimal fromAmount;

	private String toCurrency;
	private BigDecimal toAmount;

	private BigDecimal rate;

	private LocalDateTime date;
}
