package com.christielen.currencyconverter.exception;

public class ExchangeRateApiException extends Exception{

    public ExchangeRateApiException(String errorMessage) {
        super(errorMessage);
    }
}
