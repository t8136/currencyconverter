package com.christielen.currencyconverter.exception;

public class FieldValidationException extends Exception{

    public FieldValidationException(String errorMessage) {
        super(errorMessage);
    }
}
