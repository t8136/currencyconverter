package com.christielen.currencyconverter.service;


import com.christielen.currencyconverter.entity.Transaction;
import com.christielen.currencyconverter.entity.User;
import com.christielen.currencyconverter.enuns.Currency;
import com.christielen.currencyconverter.exception.ExchangeRateApiException;
import com.christielen.currencyconverter.exception.FieldValidationException;
import com.christielen.currencyconverter.json.CurrencyConverted;
import com.christielen.currencyconverter.json.CurrencyToConvert;
import com.christielen.currencyconverter.json.ExchangeRateApi;
import com.christielen.currencyconverter.repository.TransactionRepository;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CurrencyConverterService {

    @Value("${exchageRateApi.accessKey}")
    private String accessKey;

    @Autowired
    private TransactionRepository transactionRepository;

    public List<CurrencyConverted> findByUser(User user){
        log.info("List all transactions by user " + user.getUsername());
        List<CurrencyConverted> list = new ArrayList<>();
        try {
            List<Transaction> transactions =  transactionRepository.findByUser(user.getId());
            transactions.forEach(transaction -> {
                list.add(buildCurrencyConverted(transaction));
            });
        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    private void validate(CurrencyToConvert currencyToConvert) throws FieldValidationException {
        log.info("Validating parameters");
        if(Currency.valueOf(currencyToConvert.getFrom())==null){
            throw new FieldValidationException("from attribute must be (BRL, USD, EUR, JPY)");
        }
        if(Currency.valueOf(currencyToConvert.getTo())==null){
            throw new FieldValidationException("to attribute must be (BRL, USD, EUR, JPY)");
        }
        if(currencyToConvert.getAmount()==null){
            throw new FieldValidationException("amount attribute must be more or equal 0");
        }
    }

    public CurrencyConverted process(CurrencyToConvert currencyToConvert, User user) throws Exception {
        log.info("Processing convertion");
        CurrencyConverted currencyConverted = null;
        validate(currencyToConvert);
        ExchangeRateApi exchangeRateApi = getRates(currencyToConvert.getFrom());
        if(exchangeRateApi.getSuccess()){
            BigDecimal rate = exchangeRateApi.getRates().get(currencyToConvert.getTo());

            Transaction transaction = new Transaction();
            transaction.setUser(user);
            transaction.setFromCurrency(currencyToConvert.getFrom());
            transaction.setFromAmount(currencyToConvert.getAmount());
            transaction.setToCurrency(currencyToConvert.getTo());
            transaction.setToAmount(currencyToConvert.getAmount().multiply(rate));
            transaction.setRate(rate);
            transaction.setDate(LocalDateTime.now());

            log.info("Saving");
            transaction = transactionRepository.save(transaction);
            currencyConverted = buildCurrencyConverted(transaction);
        }else{
            throw new ExchangeRateApiException(exchangeRateApi.getError().toString());
        }
        return currencyConverted;
    }

    private CurrencyConverted buildCurrencyConverted(Transaction transaction){
        return new CurrencyConverted.CurrencyConvertedBuilder(transaction.getId().toString(), transaction.getUser().getId().toString())
                .fromCurrency(transaction.getFromCurrency())
                .fromAmount(transaction.getFromAmount())
                .toCurrency(transaction.getToCurrency())
                .toAmount(transaction.getToAmount())
                .rate(transaction.getRate()).build();

    }

    public ExchangeRateApi getRates(String from) throws IOException, InterruptedException {

        try {
            String url = "http://api.exchangeratesapi.io/latest?access_key=".concat(accessKey).concat("&base=".concat(from));
            log.info("Searching url: " + url);

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .header("accept", "application/json")
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            String responseBody = response.body();
            int responseStatusCode = response.statusCode();

            log.info("httpGetRequest: " + responseBody);
            log.info("httpGetRequest status code: " + responseStatusCode);

            Gson gson = new Gson();
            return gson.fromJson(responseBody, ExchangeRateApi.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw e;
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}
