package com.christielen.currencyconverter.rest;

import com.christielen.currencyconverter.entity.User;
import com.christielen.currencyconverter.json.CurrencyConverted;
import com.christielen.currencyconverter.json.CurrencyToConvert;
import com.christielen.currencyconverter.repository.UserRepository;
import com.christielen.currencyconverter.security.jwt.JwtTokenProvider;
import com.christielen.currencyconverter.service.CurrencyConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("currencyConverter")
public class CurrencyConverterController {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private CurrencyConverterService currencyConverterService;

    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/process", produces = "application/json")
    public @ResponseBody CurrencyConverted process(@RequestHeader("Authorization") String token,
                                     @RequestBody CurrencyToConvert currencyToConvert) throws Exception {

        String username = jwtTokenProvider.buscarLogin(token);
        User user = userRepository.findByUsername(username);

        return currencyConverterService.process(currencyToConvert, user);
    }

    @GetMapping(produces = "application/json")
    public @ResponseBody List<CurrencyConverted> findByUser(@RequestHeader("Authorization") String token){
        String username = jwtTokenProvider.buscarLogin(token);
        User user = userRepository.findByUsername(username);

        return currencyConverterService.findByUser(user);
    }
 }