package com.christielen.currencyconverter.rest.handler;

import com.christielen.currencyconverter.exception.*;
import com.christielen.currencyconverter.rest.form.exception.ExceptionResponseForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionResponseForm> customException(Exception e) {
        ExceptionResponseForm response = new ExceptionResponseForm();
        response.setErrorCode("INTERNAL_SERVER_ERROR");
        response.setErrorMessage("Ocorreu um erro inesperado. Por favor contate o administrador do sistema.");
        response.setTimestamp(LocalDateTime.now());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        log.error(e.getMessage(), e);
        return new ResponseEntity<ExceptionResponseForm>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(FieldValidationException.class)
    public ResponseEntity<ExceptionResponseForm> fieldValidationException(FieldValidationException e) {
        ExceptionResponseForm response = new ExceptionResponseForm();
        response.setErrorCode("INTERNAL_SERVER_ERROR");
        response.setErrorMessage(e.getMessage());
        response.setTimestamp(LocalDateTime.now());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        log.error(e.getMessage(), e);
        return new ResponseEntity<ExceptionResponseForm>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ExchangeRateApiException.class)
    public ResponseEntity<ExceptionResponseForm> exchangeRateApiException(ExchangeRateApiException e) {
        ExceptionResponseForm response = new ExceptionResponseForm();
        response.setErrorCode("INTERNAL_SERVER_ERROR");
        response.setErrorMessage(e.getMessage());
        response.setTimestamp(LocalDateTime.now());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        log.error(e.getMessage(), e);
        return new ResponseEntity<ExceptionResponseForm>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}